﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawn : MonoBehaviour
{

    private Vector3 initPos;
    private int lifes;



    void Start()
    {
        initPos = transform.position;
        lifes = 3;
    }

    private void RestartGame()
    {
        transform.position = initPos;
        lifes--;
    }

    private void GameOver()
    {
        GUILayout.Label("GAME OVER");
    }

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "spawn")
        {
            if (lifes == 0)
            {
                GameOver();
            }
            else
            {
                RestartGame();
            }
        }
    }
}
