﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBumper : MonoBehaviour
{
    public AudioClip[] audioclip;

    public int scoreValue = 300;

    void OnCollisionEnter()
    {
        ScoreManager.score += scoreValue;

        PlaySound(0);

    }
    void PlaySound(int clip) {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
}
