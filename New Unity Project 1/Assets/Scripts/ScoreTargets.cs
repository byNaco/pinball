﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTargets : MonoBehaviour
{
    public AudioClip[] audioclip;
    public int scoreValue = 600;

    void OnCollisionEnter()
    {
        ScoreManager.score += scoreValue;

        PlaySound(0);
    }
    void PlaySound(int clip)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
}