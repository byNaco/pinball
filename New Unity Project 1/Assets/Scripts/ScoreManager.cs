﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static int score = 0;
    public TextMesh display;
	
	void Update () {
        if (display) {
            display.text = score.ToString();
        }
	}
}
