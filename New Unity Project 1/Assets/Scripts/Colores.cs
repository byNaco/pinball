﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colores : MonoBehaviour
{

    // Use this for initialization
    // Guardamos todos los colores posibles en un array
    private Color[] colors = new Color[] { Color.green, Color.blue, Color.yellow };

    private IEnumerator Start()
    {
        int i = 0;

        while (true)
        {
            GetComponent<Renderer>().material.color = colors[i];
            i++;

            if (i == colors.Length)
            {
                i = 0;
            }

            // Pausamos el método durante dos segundos
            yield return new WaitForSeconds(0.5f);
        }
    }
}
